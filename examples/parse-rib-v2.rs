extern crate bgpipe_parser;
extern crate bzip2;
extern crate reqwest;

use bgpipe_parser::parser::Parser;
use bzip2::read::BzDecoder;
use std::{
    fs::File,
    io::{BufReader, Read},
};

fn count_records_remote() -> u64 {
    let url = "http://routeviews.org/bgpdata/2001.10/RIBS/rib.20011026.1648.bz2";
    let stream = reqwest::blocking::get(&url.to_owned())
        .unwrap()
        .bytes()
        .unwrap()
        .to_vec();

    let reader = BufReader::new(BzDecoder::new(&*stream));
    let parser = Parser::new(reader);

    parser.into_iter().map(|_| 1).sum()
}

fn count_add_path() -> u64 {
    let file = File::open("examples/rib.20191211.0800.bz2").unwrap();
    let reader = BzDecoder::new(&file);
    let parser = Parser::new(reader);
    parser.into_iter().map(|_x| {
        // dbg!(_x);
        1
    }).sum()
}

fn count_records_local() -> u64 {
    let file = File::open("examples/table-dump-v2-rib.20140604.1600.bz2").unwrap();
    let reader = BzDecoder::new(&file);
    let parser = Parser::new(reader);
    parser.into_iter().map(|_x| 1).sum()
}

fn main() {
    dbg!("couting locate file records");
    let local_cnt = count_records_local();
    dbg!(local_cnt);
    // dbg!("couting remote file records");
    // let remote_cnt = count_records_remote();
    // dbg!(remote_cnt);
    // dbg!("couting remote file records");
    // let remote_cnt = count_add_path();
    // dbg!(remote_cnt);
}
