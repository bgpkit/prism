# BGPKIT Parser

BGPKIT Parser provides MRT/BGP message parsing functionalities written in Rust.

## Features

BGPKIT Parser has the following features:
- performance: comparable to C-based implementations like `bgpdump` or `bgpreader`.
- supporting most of the relevant BGP RFCs.
- simple API serves as building block for more complex workflows.

## Examples

## Built with ❤️ by BGPKIT Team

BGPKIT is a small-team start-up that focus on building the best tooling for BGP data in Rust. We have 10 years of
experience working with BGP data and believe that our work can enable more companies to start keeping tracks of BGP data
on their own turf. Learn more about what services we provide at https://bgpkit.com.

<a href="https://bgpkit.com"><img src="https://bgpkit.com/Original%20Logo%20Cropped.png" alt="https://bgpkit.com/favicon.ico" width="200"/></a>
