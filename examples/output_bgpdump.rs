extern crate bgpkit_parser;
extern crate bzip2;
extern crate reqwest;

use bgpkit_parser::{BgpElem, BgpkitParser, Elementor};
use bzip2::read::BzDecoder;
use std::{
    fs::File,
};
use bgp_models::mrt::bgp4mp::Bgp4Mp;
use bgp_models::mrt::{MrtMessage, MrtRecord};
use rayon::prelude::*;
use time::PreciseTime;

fn print_bgpdump_format_updates_new() {

    let file = File::open("examples/updates.20210101.0000.bz2").unwrap();
    let reader = BzDecoder::new(&file);
    let parser = BgpkitParser::new(reader);

    for elem in parser {
        println!("{}", elem);
    }
}

fn print_bgpdump_format_ribs_v2() {

    let file = File::open("examples/table-dump-v2-rib.20140604.1600.bz2").unwrap();
    let reader = BzDecoder::new(&file);
    let parser = BgpkitParser::new(reader);

    for elem in parser {
        println!("{}", elem);
    }
}

fn print_bgpdump_format_ribs_v1() {
    let file = File::open("examples/table-dump-rib.20011026.1648.bz2").unwrap();
    let reader = BzDecoder::new(&file);
    let parser = BgpkitParser::new(reader);


    parser.into_iter()
        .collect::<Vec<BgpElem>>()
        .into_par_iter().map(|x|x.to_string()).collect::<Vec<String>>()
        .iter().for_each(|x|{
            println!("{}",x)
        })
    ;
}

fn print_bgpdump_format_ribs_v1_parallel() -> Vec<BgpElem> {
    rayon::ThreadPoolBuilder::new().num_threads(20).build_global().unwrap();
    let file = File::open("examples/table-dump-rib.20011026.1648.bz2").unwrap();
    let reader = BzDecoder::new(&file);
    let parser = BgpkitParser::new(reader);

    parser.into_iter().par_bridge().collect::<Vec<BgpElem>>()
}

fn main() {
    // print_bgpdump_format_updates_new();
    // print_bgpdump_format_ribs_v2();

    let start = PreciseTime::now();
    print_bgpdump_format_ribs_v1();
    let end = PreciseTime::now();
    println!("{} seconds for whatever you did.", start.to(end));


    // let start = PreciseTime::now();
    // print_bgpdump_format_ribs_v1_parallel();
    // let end = PreciseTime::now();
    // println!("{} seconds for whatever you did.", start.to(end));

}
