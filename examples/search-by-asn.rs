use std::fs::File;
use bzip2::read::BzDecoder;
use prism::parser::mrt_elem::Elementor;
use prism::parser::Parser;

fn main() {
    let file = File::open("examples/table-dump-v2-rib.20140604.1600.bz2").unwrap();
    let reader = BzDecoder::new(&file);
    let parser = Parser::new(reader);

    let mut elementor = Elementor::new();
    for msg in parser {
        for elem in elementor.record_to_elems(msg) {
            if let Some(origins) = &elem.origin_asn {
                if origins.contains(&13335) {
                    println!("{}", &elem);
                }
            }
        }
    }
}