extern crate bgpipe_parser;
extern crate bzip2;
extern crate reqwest;

use bgpipe_parser::parser::Parser;
use bzip2::read::BzDecoder;
use std::{
    fs::File,
};
use bgpipe_parser::models::{MrtMessage, Bgp4Mp};


fn count_records_local(new: bool) -> u64 {

    let file = match new {
        true => File::open("examples/updates.20210101.0000.bz2"),
        false => File::open("examples/updates.20011026.1352.bz2"),
    }.unwrap();
    let reader = BzDecoder::new(&file);
    let parser = Parser::new(reader);
    // parser.into_iter().map(|_| 1).sum()

    let mut msgs = vec!();
    for msg in parser {
        match &msg.message {
            MrtMessage::Bgp4Mp(msg) => {
                match msg {
                    Bgp4Mp::Bgp4MpStateChange(_) => {}
                    Bgp4Mp::Bgp4MpStateChangeAs4(_) => {}
                    Bgp4Mp::Bgp4MpMessage(_) => {}
                    Bgp4Mp::Bgp4MpMessageLocal(_) => {}
                    Bgp4Mp::Bgp4MpMessageAs4(_) => {}
                    Bgp4Mp::Bgp4MpMessageAs4Local(_) => {}
                }
                //println!("{}", msg);
            }
            _ => {}
        }
        msgs.push(msg);
    }
    return msgs.len() as u64;
}

fn main() {
    /*
    dbg!("counting updates from older file in 2001");
    let local_cnt = count_records_local(false);
    dbg!(local_cnt);
     */

    dbg!("counting updates from newer file in 2021");
    let local_cnt = count_records_local(true);
    dbg!(local_cnt);
    // dbg!("couting remote file records");
    // let remote_cnt = count_records_remote();
    // dbg!(remote_cnt);
}
