extern crate bgpipe_parser;
extern crate bzip2;
extern crate reqwest;

use bgpipe_parser::parser::Parser;
use bzip2::read::BzDecoder;
use std::{
    fs::File,
    io::BufReader,
};
use std::collections::HashSet;
use bgp_models::mrt::MrtMessage;

#[allow(dead_code)]
fn count_records_remote() -> u64 {
    let url = "http://routeviews.org/bgpdata/2001.10/RIBS/rib.20011026.1648.bz2";
    let stream = reqwest::blocking::get(&url.to_owned())
        .unwrap()
        .bytes()
        .unwrap()
        .to_vec();

    let reader = BufReader::new(BzDecoder::new(&*stream));
    let parser = Parser::new(reader);

    parser.into_iter().map(|_| 1).sum()
}

fn count_records_local() -> u64 {
    let file = File::open("examples/table-dump-rib.20011026.1648.bz2").unwrap();
    let reader = BzDecoder::new(&file);
    let parser = Parser::new(reader);
    // parser.into_iter().map(|_| 1).sum()

    let mut msg_count = 0;
    // let mut peer_ips = HashSet::new();
    for msg in parser {
        msg_count+=1;
    }
    return msg_count as u64;
}

fn main() {
    dbg!("couting locate file records");
    let local_cnt = count_records_local();
    dbg!(local_cnt);
    // dbg!("couting remote file records");
    // let remote_cnt = count_records_remote();
    // dbg!(remote_cnt);
}
